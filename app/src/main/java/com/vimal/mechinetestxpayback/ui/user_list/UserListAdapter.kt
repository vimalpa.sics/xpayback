package com.vimal.mechinetestxpayback.ui.user_list

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.vimal.mechinetestxpayback.R
import com.vimal.mechinetestxpayback.data.models.user_list_model.Users
import com.vimal.mechinetestxpayback.databinding.UserItemsBinding

class UserListAdapter(private val onSelect: (Int?) -> Unit) : RecyclerView.Adapter<UserListVH>() {
    //    var users = mutableListOf<Users>()
    private var users: List<Users> = ArrayList()


    @SuppressLint("NotifyDataSetChanged")
    fun setMovieList(users: List<Users>) {
        this.users = this.users + users

//        notifyDataSetChanged()
        notifyItemRangeInserted(this.users.size - users.size, users.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            UserListVH =
        UserListVH(UserItemsBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: UserListVH, position: Int) {
        holder.apply {
            bind(users[position])
        }
        holder.binding.baseLayout.setOnClickListener {
            onSelect(users[position].id)
        }
    }

    override fun getItemCount(): Int {
        return users.size
    }




    companion object {
        @JvmStatic
        @BindingAdapter("imageurl")
        fun loadImage(thumbs: ImageView, url: String) {
            Glide.with(thumbs)
                .load(url)
                .circleCrop()
                .placeholder(R.drawable.ic_launcher_foreground)
                .error(R.drawable.ic_launcher_foreground)
                .fallback(R.drawable.ic_launcher_foreground)
                .into(thumbs)
        }
    }


}
