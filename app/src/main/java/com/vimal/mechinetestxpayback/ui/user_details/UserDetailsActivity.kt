package com.vimal.mechinetestxpayback.ui.user_details

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.vimal.mechinetestxpayback.R
import com.vimal.mechinetestxpayback.data.models.user_details_model.UserDetails
import com.vimal.mechinetestxpayback.databinding.ActivityUserDetailsBinding

class UserDetailsActivity : AppCompatActivity(),UserDetailsListener{
    lateinit var binding: ActivityUserDetailsBinding
    lateinit var viewModel: UserDetailsVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val id = intent.getIntExtra("id",0)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_details)

        viewModel=ViewModelProvider(this)[UserDetailsVM::class.java]
        binding.userDetails=viewModel

        viewModel.getUserDetails(id)
        viewModel.userDetailsListener=this


        viewModel.userDetails.observe(this) {
                binding.userData=it
        }

        binding.back.setOnClickListener {
            finish()
        }


    }


    override fun started() {

    }

    override fun success(userDetails: UserDetails?) {
        binding.baseLayout.visibility=View.VISIBLE
        Glide.with(binding.userImage)
            .load(userDetails?.image)
            .circleCrop()
            .placeholder(R.drawable.ic_launcher_foreground)
            .error(R.drawable.ic_launcher_foreground)
            .fallback(R.drawable.ic_launcher_foreground)
            .into(binding.userImage)
    }



    override fun failure(message: String) {
        TODO("Not yet implemented")
    }
}