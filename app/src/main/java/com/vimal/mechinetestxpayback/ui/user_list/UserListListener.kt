package com.vimal.mechinetestxpayback.ui.user_list

import com.vimal.mechinetestxpayback.data.models.user_list_model.UserDataList

interface UserListListener {

    fun started()
    fun success()
    fun failure(message: String)
}