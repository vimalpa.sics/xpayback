package com.vimal.mechinetestxpayback.ui.user_list

import android.content.Intent
import android.nfc.tech.MifareUltralight.PAGE_SIZE
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vimal.mechinetestxpayback.R
import com.vimal.mechinetestxpayback.databinding.ActivityMainBinding
import com.vimal.mechinetestxpayback.ui.user_details.UserDetailsActivity

class UserListActivity : AppCompatActivity(), UserListListener {

    private val TAG = "MainActivity"
    lateinit var binding: ActivityMainBinding
    lateinit var  viewModel:UserListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        viewModel = ViewModelProvider(this)[UserListViewModel::class.java]

        viewModel.userListListener = this

        setAdapter()
        setScrollListener()
        viewModel.getUserList(0)

    }

    private fun setScrollListener() {
        val layoutManager = LinearLayoutManager(this)
        binding.recyclerView.layoutManager = layoutManager

        binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val visibleItemCount = layoutManager.childCount
                val totalItemCount = layoutManager.itemCount
                val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
                if (!viewModel.isLoading && !viewModel.isLastPage) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= PAGE_SIZE
                    ) {
                        viewModel.isLoading = true
                        viewModel.getUserList(viewModel.skip + viewModel.limit)
                    }
                }
            }
        })

    }

    private fun setAdapter() {

        val adapter = UserListAdapter {
            val intent = Intent(this, UserDetailsActivity::class.java)
            intent.putExtra("id", it)
            startActivity(intent)

        }
        binding.recyclerView.adapter = adapter
        viewModel.userList.observe(this) {
            Log.d(TAG, "onCreate: $it")
            adapter.setMovieList(it)
        }
    }


    override fun started() {
        binding.progressBar.visibility = View.VISIBLE
    }

    override fun success() {
        binding.progressBar.visibility = View.GONE

    }

    override fun failure(message: String) {
        binding.progressBar.visibility = View.GONE
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }


}