package com.vimal.mechinetestxpayback.ui.user_details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vimal.mechinetestxpayback.data.models.user_details_model.UserDetails
import com.vimal.mechinetestxpayback.data.models.user_list_model.UserDataList
import com.vimal.mechinetestxpayback.data.models.user_list_model.Users
import com.vimal.mechinetestxpayback.data.remote.ApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserDetailsVM : ViewModel() {
    var userDetailsListener: UserDetailsListener? = null
    val userDetails = MutableLiveData<UserDetails>()

    fun getUserDetails(id: Int) {
        userDetailsListener?.started()
        val  userDataList= MutableLiveData<UserDataList>()

        ApiService.invoke().getUsersDetails(id).enqueue(object : Callback<UserDetails> {
            override fun onResponse(call: Call<UserDetails>, response: Response<UserDetails>) {
                if (response.code()==200) {
                    userDetailsListener?.success(response.body())
                    userDetails.value=response.body()
                }
                else{
                    userDetailsListener?.failure("Some Error Occurred")

                }

            }

            override fun onFailure(call: Call<UserDetails>, t: Throwable) {
                userDetailsListener?.failure(t.message.toString())
            }


        })

    }


}