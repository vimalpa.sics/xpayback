package com.vimal.mechinetestxpayback.ui.user_list

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.vimal.mechinetestxpayback.data.models.user_list_model.UserDataList
import com.vimal.mechinetestxpayback.data.models.user_list_model.Users
import com.vimal.mechinetestxpayback.databinding.UserItemsBinding

class UserListVH(val binding: UserItemsBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(users: Users) {
        binding.p= users
    }

}
