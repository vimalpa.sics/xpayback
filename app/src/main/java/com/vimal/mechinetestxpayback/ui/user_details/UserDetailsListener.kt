package com.vimal.mechinetestxpayback.ui.user_details

import com.vimal.mechinetestxpayback.data.models.user_details_model.UserDetails

interface UserDetailsListener {
    fun started()
    fun success(userDetails: UserDetails?)
    fun failure(message: String)
}