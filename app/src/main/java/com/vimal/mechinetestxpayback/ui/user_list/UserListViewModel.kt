package com.vimal.mechinetestxpayback.ui.user_list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vimal.mechinetestxpayback.data.models.user_list_model.UserDataList
import com.vimal.mechinetestxpayback.data.models.user_list_model.Users
import com.vimal.mechinetestxpayback.data.remote.ApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserListViewModel(
): ViewModel() {
    var isLoading: Boolean=false
    var isLastPage: Boolean=false
    var limit=10;
    var skip=0;
    var userListListener:UserListListener?=null
    val userList = MutableLiveData<List<Users>>()

    fun getUserList(skip: Int) {
        userListListener?.started()
        this.skip=skip
        val  userDataList= MutableLiveData<UserDataList>()

        ApiService.invoke().getUsersList(limit,skip).enqueue(object : Callback<UserDataList> {
            override fun onResponse(call: Call<UserDataList>, response: Response<UserDataList>) {
               if (response.code()==200&& response.body()?.users?.size!!>0) {
                   userListListener?.success()
                   userDataList.value = response.body()
                   userList.value= response.body()?.users
                   isLoading=false
                   if (skip+limit>= response.body()!!.total!!){
                       isLastPage=true
                   }
               }
                else{
                   userListListener?.failure("Some Error Occurred")

               }

            }

            override fun onFailure(call: Call<UserDataList>, t: Throwable) {
                userListListener?.failure(t.message.toString())
            }


        })

    }


}



