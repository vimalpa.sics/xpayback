package com.vimal.mechinetestxpayback.data.remote

import com.vimal.mechinetestxpayback.data.models.user_details_model.UserDetails
import com.vimal.mechinetestxpayback.data.models.user_list_model.UserDataList
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.*
import java.util.concurrent.TimeUnit

interface ApiService {


    @GET(Urls.USERS)
    fun getUsersList(@Query("limit") limit: Int, @Query("skip") skip: Int)
            :Call<UserDataList>



    @GET(Urls.USERS+"/{id}")
     fun getUsersDetails(@Path("id") userId: Int): Call<UserDetails>



    companion object{

        private val interceptor = HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.BODY)

        private val okHttpClient = OkHttpClient.Builder()
            .addNetworkInterceptor(interceptor) // same for .addInterceptor(...)
            .connectTimeout(1, TimeUnit.MINUTES) //Backend is really slow
            .writeTimeout  (  1, TimeUnit.MINUTES)
            .readTimeout   (   1, TimeUnit.MINUTES)
            .build()

        operator fun invoke() :ApiService{
            return Retrofit.Builder()
                .baseUrl(Urls.BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()
                .create(ApiService::class.java)
        }


    }
}