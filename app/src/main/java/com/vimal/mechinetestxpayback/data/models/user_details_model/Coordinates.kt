package com.vimal.mechinetestxpayback.data.models.user_details_model

import com.google.gson.annotations.SerializedName


data class Coordinates (

  @SerializedName("lat" ) var lat : Double? = null,
  @SerializedName("lng" ) var lng : Double? = null

)