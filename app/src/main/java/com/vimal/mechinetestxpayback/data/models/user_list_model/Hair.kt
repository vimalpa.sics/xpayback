package com.vimal.mechinetestxpayback.data.models.user_list_model

import com.google.gson.annotations.SerializedName


data class Hair (

  @SerializedName("color" ) var color : String? = null,
  @SerializedName("type"  ) var type  : String? = null

)